variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-apps-api-devops"
}

variable "contact" {
  default = "mt.himal@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS Postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}
